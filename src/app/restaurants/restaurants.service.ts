import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Restaurant } from './restaurant/restaurant.model'
import { Review } from '../restaurant-detail/reviews/review/review.model'
import { MenuItem } from '../restaurant-detail/menu-item/menu-item.model'
import { MEAT_API } from '../app.api'

@Injectable()
export class RestaurantsService{

    constructor(private http: HttpClient ){
    }

    restaurants(search?: string): Observable<Restaurant[]>{
      let parametro: HttpParams = undefined
      if(search){
        parametro = new HttpParams().append('q',search)
      }
      return this.http.get<Restaurant[]>(`${MEAT_API}/restaurants`,{params: parametro})
    }

    resraurantById(id: string): Observable<Restaurant>{
      return this.http.get<Restaurant>(`${MEAT_API}/restaurants/${id}`)
    }

    reviewOfRestaurant(id: string): Observable<Review>{
      return this.http.get<Review>(`${MEAT_API}/restaurants/${id}/reviews`)
    }

    menuOfRestaurant(id: string): Observable<MenuItem[]>{
      return this.http.get<MenuItem[]>(`${MEAT_API}/restaurants/${id}/menu`)
    }
}